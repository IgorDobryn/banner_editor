require 'zip/zip'
require 'zip/zipfilesystem'

class BannerArchiver
  FILE_DIR = "#{Rails.root}/tmp/banners/"

  def initialize banner
    @banner = banner
  end

  def archive_data
    write_zip!
  end

  def write_zip!
    file = File.open("#{base_file_name}.zip", 'w')

    Zip::ZipOutputStream.open(file.path) do |zos|
      [:html, :css, :js].each do |component|
        zos.put_next_entry("#{@banner.template}.#{component}")
        zos.print public_send "#{component}_data"
      end
    end
    file.close

    file
  end

  def css_data
    return @css_data if @css_data

    compress_css!

    @css_data = IO.read css_file_name
  end

  def compress_css!
    find_asset("#{@banner.template}_banner_manifest.css").write_to css_file_name
  end

  def find_asset asset_file_name
    BannerEditor::Application.assets.find_asset asset_file_name
  end

  def css_file_name
    @css_file_name ||= "#{base_file_name}.css"
  end

  def js_data
    return @js_data if @js_data

    compress_js!

    @js_data = IO.read js_file_name
  end

  def compress_js!
    find_asset("#{@banner.template}_banner_manifest.js").write_to js_file_name
  end

  def js_file_name
    @js_file_name ||= "#{base_file_name}.js"
  end

  def html_data
    @html_data ||= ActionController::Base.new.render_to_string partial: "banners/templates/#{@banner.template}",
                                                               layout: "#{@banner.template}_banner_layout",
                                                               locals: @banner.banner_attributes_hash
  end

  def base_file_name
    @file_name ||= "#{FILE_DIR}#{@banner.template}_banner"
  end
end