class TemplateGenerator

  def initialize id
    @id = id
  end

  def generate!
    banner = Banner.find_by_id(@id)
    raise 'Untemplatable entity.' if !banner || !banner.default?

    old_template = banner.template

    banner.generate_template_name!
    raise 'Template already exists.' if File.exist?("#{Rails.root}/app/views/banners/templates/_#{banner.template}.haml")

    copy_file templates_dir, "_#{old_template}.haml", "_#{banner.template}.haml" do |destination, content|
      destination.write(content.gsub!(/\.#{old_template}-banner/, ".#{banner.template}-banner"))
    end

    copy_file layouts_dir, "_#{old_template}_banner_layout.haml", "_#{banner.template}_banner_layout.haml" do |destination, content|
      destination.write(content.gsub!(/#{old_template}/, banner.template))
    end

    copy_file stylesheets_dir, "#{old_template}_banner_manifest.css.scss", "#{banner.template}_banner_manifest.css.scss" do |destination, content|
      destination.write(content.gsub!(/#{old_template}_banner/, "#{banner.template}_banner"))
    end

    copy_file stylesheets_dir, "_#{old_template}_banner.css.scss", "_#{banner.template}_banner.css.scss" do |destination, content|
      destination.write(content.gsub!(/.#{old_template}-banner/, ".#{banner.template}-banner"))
    end

    copy_file javascripts_dir, "#{old_template}_banner_manifest.js", "#{banner.template}_banner_manifest.js" do |destination, content|
      destination.write(content.gsub!(/#{old_template}_banner/, "#{banner.template}_banner"))
    end

    copy_file javascripts_dir, "banners/#{old_template}_banner.js.coffee", "banners/#{banner.template}_banner.js.coffee" do |destination, content|
      res = content.gsub!(/#{old_template.capitalize}Banner/, "#{banner.template.capitalize}Banner")
      res = res.gsub!(/\.#{old_template}-banner/, ".#{banner.template}-banner")

      destination.write(res)
    end

    banner.save!
  end

  def copy_file dir, src_file_name, dst_file_name, &block
    File.open dir + src_file_name do |source|
      File.open dir + dst_file_name, 'w+' do |destination|
        block.call destination, source.read
      end
    end
  end

  def templates_dir
    "#{Rails.root}/app/views/banners/templates/"
  end

  def layouts_dir
    "#{Rails.root}/app/views/layouts/"
  end

  def stylesheets_dir
    "#{Rails.root}/app/assets/stylesheets/"
  end

  def javascripts_dir
    "#{Rails.root}/app/assets/javascripts/"
  end

end