#= require banners/base

class Banners.DefaultBanner extends Banners.Base
  animationTime: 3000

  constructor: (container)->
    super container
    @actions = [(=> @messageAnimation()), (=> @boxAnimation())]

  run: ->
    super()

  messageAnimation: ->
    @message().fadeIn @animationTime, =>
      setTimeout (=> @message().fadeOut @animationTime), @animationTime

  message: ->
    @_message ||= @banner.find('.message')

  boxAnimation: ->
    box = @banner.find('.box')
    imgContainer = @banner.find('.img-container')
    box.animate {left: parseInt(imgContainer.attr('width')) - box.width()}, 3 * @animationTime


$ ->
  $('.default-banner').each (index, element)->new Banners.DefaultBanner($(element)).run()
