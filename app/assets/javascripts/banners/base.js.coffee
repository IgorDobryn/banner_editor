class Banners.Base
  actions: []

  constructor: (container)->
    @banner = container

  run: ->
    action() for action in @actions

    @