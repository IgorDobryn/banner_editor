#= require ./default_banner

class Banners.DefaultBannerEditor extends Banners.DefaultBanner

  constructor: (container)->
    super(container)

    @editMessageIcon().click =>
      @editMessageIcon().hide()
      @message().hide()
      @messageContainer().append(@generateMessageInput())
      @messageInput().focus()

  editMessageIcon: ->
    @_editMessageIcon ||= @banner.find('.edit-message')

  messageContainer: ->
    @_messageContainer ||= @banner.find('.message-container')

  generateMessageInput: ->
    $('<input name="default_banner[message]"/>').val(@message().text()).keypress (event)=>
      if (event.keyCode is 13)
        @showMessage()
        event.preventDefault()
        event.stopPropagation()

  messageInput: ->
    @messageContainer().find('input')

  showMessage: ->
    @message().html(@messageInput().val()).show()
    @formMessageInput().val(@messageInput().val())
    @messageInput().remove()
    @editMessageIcon().show()

  run: ->
    @banner.resizable
      stop: (event, ui)=>
        @formWidthInput().val ui.size.width
        @formHeightInput().val ui.size.height

  formMessageInput: ->
    @_formMessageInput ||= @findFormInput('message')

  formWidthInput: ->
    @_formWidthInput ||= @findFormInput('width')

  formHeightInput: ->
    @_formHeightInput ||= @findFormInput('height')

  findFormInput: (name)->
    nameInput = @banner.find("form input[value='#{name}']")
    @banner.find "##{nameInput.attr('id').replace('name', 'value')}"

$ ->
  $('.default-banner form').parents('.default-banner').each (index, element)->
    new Banners.DefaultBannerEditor($(element)).run()
