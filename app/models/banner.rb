class Banner < ActiveRecord::Base
  DEFAULTS = { template: 'default' }

  belongs_to :user
  has_many :banner_attributes, dependent: :destroy

  attr_accessible :banner_attributes_attributes
  accepts_nested_attributes_for :banner_attributes

  class << self
    def default
      @banner ||= Banner.new(banner_attributes_attributes: BannerAttribute.default_attributes_hash)
    end
  end

  def banner_attributes_hash
    banner_attributes.inject({banner: self}) do |acc, attribute|
      acc[attribute.name.to_sym] = attribute.value
      acc
    end
  end

  def template
    self.attributes['template'] || DEFAULTS[:template]
  end

  def default?
    self.template == DEFAULTS[:template]
  end

  def generate_template_name!
    self.template = "banner_#{self.id}"
  end
end
