class BannerAttribute < ActiveRecord::Base
  DEFAULTS = { width: 320, height: 50, message: 'Hello World!' }

  belongs_to :banner
  attr_accessible :name, :value

  class << self

    def default_attributes_hash
      @default_attributes_hash ||= DEFAULTS.map { |key, value| {name: key, value: value} }
    end

  end

end
