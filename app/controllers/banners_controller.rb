class BannersController < ApplicationController
  load_and_authorize_resource :banner, through: :current_user

  def create
    if @banner.save
      redirect_to root_url
    end
  end

  def show
    respond_to do |format|
      format.html { render :show }
      format.zip  { send_file BannerArchiver.new(@banner).archive_data.path, type: 'application/zip', disposition: 'attachment' }
    end
  end

end