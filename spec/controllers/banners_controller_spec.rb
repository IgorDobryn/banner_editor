require 'spec_helper'

describe BannersController do

  context '[authorization]' do
    let(:user) {FactoryGirl.create :user}

    context '[create]' do
      it 'should allow user to create banners' do
        sign_in(user)

        post :create, banner: {}

        response.should redirect_to(root_path)
      end

      it 'should not allow visitor to create banners' do
        -> { post :create, banner: {} }.should raise_error(CanCan::AccessDenied)
      end
    end

    context '[show]' do
      before do
        @banner = FactoryGirl.create :banner, user: user
      end

      it 'should show banner to user' do
        sign_in(user)

        get :show, id: @banner.id

        response.should be_success
      end

      it 'should not show banner to visitor' do
        -> { get :show, id: @banner.id }.should raise_error(CanCan::AccessDenied)
      end
    end
  end
end
