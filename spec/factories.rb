FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user#{n}@mailinator.com" }
    password 'password'
    password_confirmation 'password'
  end

  factory :banner do
    association :user

    ignore do
      attributes Hash.new
    end

    after(:create) do |banner, evaluator|
      evaluator.attributes.each do |name, value|
        create :banner_attribute, banner: banner, name: name, value: value
      end
    end
  end

  factory :banner_attribute do
    association :banner
    sequence(:name) { |n| "attribute#{n}" }
    sequence(:value) { |n| "value#{n}" }
  end
end