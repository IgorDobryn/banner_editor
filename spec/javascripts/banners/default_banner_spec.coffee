#= require ../helpers/SpecHelper
#= require banners/default_banner

describe 'Banners.DefaultBanner', ->
  describe '#messageAnimation', ->
    it 'should call correct animating actions for message', ->
      jasmine.Clock.useMock()

      spy = jasmine.createSpyObj 'spy', ['find', 'fadeIn', 'fadeOut']
      spy.find.andReturn spy
      spy.fadeIn.andCallFake (time, func)-> func()
      spy.fadeOut.andReturn spy

      banner = new Banners.DefaultBanner(spy)
      banner.messageAnimation()

      jasmine.Clock.tick(3000)

      expect(spy.find).toHaveBeenCalledWith('.message')
      expect(spy.fadeIn).toHaveBeenCalled()
      expect(spy.fadeOut).toHaveBeenCalledWith(3000)

  describe '#boxAnimation', ->
    it 'should call correct animating actions for box', ->
      spy = jasmine.createSpyObj 'spy', ['find', 'attr']
      box = jasmine.createSpyObj 'box', ['width', 'animate']

      spy.find.andCallFake (selector)=> if selector is '.box' then box else spy

      spy.attr.andReturn 300
      box.width.andReturn 150

      banner = new Banners.DefaultBanner(spy)
      banner.boxAnimation()

      expect(box.animate).toHaveBeenCalledWith({left: 150}, 3 * 3000)

  describe '#run', ->
    it 'should call correct animations', ->
      banner = new Banners.DefaultBanner()

      spyOn(banner, 'boxAnimation')
      spyOn(banner, 'messageAnimation')

      banner.run()

      expect(banner.messageAnimation).toHaveBeenCalled()
      expect(banner.boxAnimation).toHaveBeenCalled()
