#= require ../helpers/SpecHelper
#= require banners/default_banner_editor

describe 'Banners.DefaultBannerEditor', ->
  describe '#run', ->
    it 'should call resizeable', ->
      spy1 = jasmine.createSpyObj('spy1', ['resizable', 'find', 'click'])
      spy1.find.andReturn(spy1)

      banner = new Banners.DefaultBannerEditor(spy1)
      banner.run()

      expect(spy1.resizable).toHaveBeenCalled()