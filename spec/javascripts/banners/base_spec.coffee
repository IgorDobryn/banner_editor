#= require ../helpers/SpecHelper
#= require banners/base

describe 'Banners.Base', ->
  banner = undefined


  beforeEach ->
    banner = new Banners.Base('selector')

  describe '#run', ->
    it 'should call chain of actions', ->
      spy1 = jasmine.createSpy('spy1')
      spy2 = jasmine.createSpy('spy2')

      banner.actions = [spy1, spy2]
      banner.run()

      expect(spy1).toHaveBeenCalled()
      expect(spy2).toHaveBeenCalled()