require 'spec_helper'

describe Banner do
  describe '#template' do
    it 'should return default template if it is not set' do
      banner = Banner.new

      banner.template.should == 'default'

      banner.template = 'new_template'
      banner.template.should_not == 'default'
    end
  end

  describe '#default' do
    it 'should return banner with default attributes' do
      default = Banner.default

      attributes = default.banner_attributes.map {|attribute| {attribute.name => attribute.value}}

      attributes.length.should == 3

      attributes.should include({width: 320})
      attributes.should include({height: 50})
      attributes.should include({message: 'Hello World!'})
    end
  end

  describe '#banner_attributes_hash' do
    it 'should return associated attributes converted to name/value hash' do
      banner = FactoryGirl.create :banner
      FactoryGirl.create :banner_attribute, banner: banner, name: 'title', value: 'the best banner'
      FactoryGirl.create :banner_attribute, banner: banner, name: 'source', value: 'www.example.com'

      hash = banner.banner_attributes_hash

      hash.length.should == 3
      hash[:title].should == 'the best banner'
      hash[:source].should == 'www.example.com'
      hash[:banner].should == banner
    end
  end

end
