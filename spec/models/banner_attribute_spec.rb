require 'spec_helper'

describe BannerAttribute do
  describe '#default_attributes_hash' do
    it 'should return array of default attributes converted to name/value hash' do
      attributes = BannerAttribute.default_attributes_hash

      attributes.length.should == 3
      attributes.should include({name: :width, value: 320})
      attributes.should include({name: :height, value: 50})
      attributes.should include({name: :message, value: 'Hello World!'})
    end
  end
end
