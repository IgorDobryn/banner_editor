require 'spec_helper'
require 'requests/request_helper'

describe "Dashboard" do
  describe "GET /dashboard" do
    it 'should show default banner' do
      visit root_path

      page.should have_selector('#new_banner .message', text: 'Hello World!')
      page.find('#new_banner img')['src'].should == "http://placehold.it/320x50"
    end

    it 'should edit banner message', js: true do
      user = FactoryGirl.create :user
      sign_in user

      visit root_path

      page.execute_script('$("#new_banner .edit-message").click()')
      fill_in('default_banner[message]', with: 'New Banner Message')
      page.execute_script('$("#new_banner input").trigger(new jQuery.Event("keypress", {keyCode: 13}))')

      page.should have_selector('#new_banner .message', text: 'New Banner Message')
    end

    it 'should create new user banner' do
      user = FactoryGirl.create(:user)
      sign_in(user)

      click_button 'Save to own collection'

      banner = Banner.last

      page.should have_selector("#banner_#{banner.id} .message", text: 'Hello World!')
      page.find("#banner_#{banner.id} img")['src'].should == "http://placehold.it/320x50"
    end

    it 'should display user banners' do
      user = FactoryGirl.create(:user)
      banner = FactoryGirl.create :banner, user: user, attributes: {message: 'Test banner', width: '130', height: '280'}

      sign_in user

      visit root_path

      page.should have_selector("#banner_#{banner.id} .message", text: 'Test banner')
      page.find("#banner_#{banner.id} img")['src'].should == "http://placehold.it/130x280"
    end

    it 'should download banner zip' do
      user = FactoryGirl.create(:user)
      banner = FactoryGirl.create :banner, user: user, attributes: {message: 'Test banner', width: '130', height: '280'}

      sign_in user

      visit root_path

      within("#banner_#{banner.id}") do
        click_link('Download zip')
      end

      page.response_headers['Content-Disposition'].should == 'attachment; filename="default_banner.zip"'
      page.response_headers['Content-Type'].should == 'application/zip'
    end
  end
end
