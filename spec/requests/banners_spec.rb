require 'spec_helper'
require 'requests/request_helper'

describe "Banners" do
  describe "GET /banners/:id" do
    it 'should show default banner' do
      user = FactoryGirl.create :user
      banner = FactoryGirl.create :banner, user: user, attributes: {message: 'Test banner', width: '130', height: '280'}

      sign_in user

      visit banner_path(banner)

      page.should have_selector(".default-banner .message", text: 'Test banner')
      page.find(".default-banner img")['src'].should == "http://placehold.it/130x280"
      page.should_not have_selector('form')
    end
  end
end
