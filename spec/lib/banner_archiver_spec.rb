require 'spec_helper'

describe BannerArchiver do

  let(:banner) { FactoryGirl.create :banner, attributes: {message: 'Test banner', width: '130', height: '280'} }
  let(:banner_archiver) { BannerArchiver.new banner }


  describe '#find_asset' do
    it 'should delegate find asset to application Sprockets Environment' do
      BannerEditor::Application.assets.expects(:find_asset)

      banner_archiver.find_asset 'some_file'
    end
  end

  describe '#base_file_name' do
    it 'should return base file name' do
      banner_archiver.base_file_name.should == "#{Rails.root}/tmp/banners/default_banner"
    end
  end

  describe '#js_file_name' do
    it 'should return js file name' do
      banner_archiver.js_file_name.should == "#{Rails.root}/tmp/banners/default_banner.js"
    end
  end

  describe '#css_file_name' do
    it 'should return css file name' do
      banner_archiver.css_file_name.should == "#{Rails.root}/tmp/banners/default_banner.css"
    end
  end

  describe '#write_zip!' do
    it 'should write banner components to zip file' do
      html_data = mock 'html_data'
      css_data = mock 'css_data'
      js_data = mock 'js_data'

      banner_archiver.stubs(:html_data).returns html_data
      banner_archiver.stubs(:css_data).returns css_data
      banner_archiver.stubs(:js_data).returns js_data

      stream = mock 'stream'
      stream.expects(:put_next_entry).with 'default.html'
      stream.expects(:put_next_entry).with 'default.css'
      stream.expects(:put_next_entry).with 'default.js'

      stream.expects(:print).with html_data
      stream.expects(:print).with css_data
      stream.expects(:print).with js_data
      stream.expects :close

      file = mock path: 'some_file', close: nil

      File.expects(:open).with("#{Rails.root}/tmp/banners/default_banner.zip", 'w').returns file

      Zip::ZipOutputStream.expects(:new).with('some_file').returns stream

      banner_archiver.write_zip!.should == file
    end
  end

  describe '#compress_css!' do
    it 'should write css to external file' do
      asset = mock 'asset'
      asset.expects(:write_to).with "#{Rails.root}/tmp/banners/default_banner.css"

      banner_archiver.expects(:find_asset).with('default_banner_manifest.css').returns asset

      banner_archiver.compress_css!
    end
  end

  describe '#compress_js!' do
    it 'should write js to external file' do
      asset = mock 'asset'
      asset.expects(:write_to).with "#{Rails.root}/tmp/banners/default_banner.js"

      banner_archiver.expects(:find_asset).with('default_banner_manifest.js').returns asset

      banner_archiver.compress_js!
    end
  end

  describe '#css_data' do
    it 'should read css data from file' do
      banner_archiver.expects :compress_css!

      data = mock 'data'

      IO.expects(:read).with("#{Rails.root}/tmp/banners/default_banner.css").returns data

      banner_archiver.css_data.should == data

    end
  end

  describe '#js_data' do
    it 'should read js data from file' do
      banner_archiver.expects :compress_js!

      data = mock 'data'

      IO.expects(:read).with("#{Rails.root}/tmp/banners/default_banner.js").returns data

      banner_archiver.js_data.should == data
    end
  end

  describe '#html_data' do
    it 'should read js data from file' do
      data = mock 'data'
      ac = mock 'ac'
      ac.expects(:render_to_string ).with(partial: "banners/templates/default",
                                          layout: 'default_banner_layout',
                                          locals: {height: '280', message: 'Test banner', width: '130', banner: banner}).returns data

      ActionController::Base.stubs(:new).returns ac

      banner_archiver.html_data.should == data
    end
  end

  describe '#archive_data' do
    it 'should return archive_data' do
      data = mock 'data'
      banner_archiver.expects(:write_zip!).returns data

      banner_archiver.archive_data.should == data
    end
  end

end
