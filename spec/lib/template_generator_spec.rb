require 'spec_helper'

describe TemplateGenerator do

  let(:banner) { FactoryGirl.create :banner, attributes: {message: 'Test banner', width: '130', height: '280'} }
  let(:template_generator) { TemplateGenerator.new banner.id }

  describe '#templates_dir' do
    it 'should return correct templates dir' do
      template_generator.templates_dir.should == "#{Rails.root}/app/views/banners/templates/"
    end
  end

  describe '#layouts_dir' do
    it 'should return correct layouts dir' do
      template_generator.layouts_dir.should == "#{Rails.root}/app/views/layouts/"
    end
  end

  describe '#stylesheets_dir' do
    it 'should return correct stylesheets dir' do
      template_generator.stylesheets_dir.should == "#{Rails.root}/app/assets/stylesheets/"
    end
  end

  describe '#javascripts_dir' do
    it 'should return correct javascripts dir' do
      template_generator.javascripts_dir.should == "#{Rails.root}/app/assets/javascripts/"
    end
  end

  describe '#generate!' do
    let(:fixtures_dir) { "#{Rails.root}/spec/fixtures/" }
    before do
      template_generator.expects(:templates_dir).returns(fixtures_dir)
      template_generator.expects(:layouts_dir).returns(fixtures_dir)
      template_generator.expects(:stylesheets_dir).returns(fixtures_dir).times(2)
      template_generator.expects(:javascripts_dir).returns(fixtures_dir).times(2)

      template_generator.generate!
      banner.reload
    end

    after do
      FileUtils.remove "#{fixtures_dir}_#{banner.template}.haml"
      FileUtils.remove "#{fixtures_dir}_#{banner.template}_banner.css.scss"
      FileUtils.remove "#{fixtures_dir}_#{banner.template}_banner_layout.haml"
      FileUtils.remove "#{fixtures_dir}#{banner.template}_banner_manifest.css.scss"
      FileUtils.remove "#{fixtures_dir}#{banner.template}_banner_manifest.js"
      FileUtils.remove "#{fixtures_dir}banners/#{banner.template}_banner.js.coffee"
    end

    it 'should generate correct template file' do
      File.open "#{fixtures_dir}_#{banner.template}.haml" do |file|
       file.read.should == ".#{banner.template}-banner"
      end
    end

    it 'should generate correct layout file' do
      File.open "#{fixtures_dir}_#{banner.template}_banner_layout.haml" do |file|
        file.readline.should == "%link{href: \"#{banner.template}_banner_manifest.css\", media: \"all\", rel: \"stylesheet\", type: \"text/css\"}\n"
        file.readline.should == "%script{type: 'text/javascript', src: '#{banner.template}_banner_manifest.js'}"
      end
    end

    it 'should generate correct css manifest file' do
      File.open "#{fixtures_dir}#{banner.template}_banner_manifest.css.scss" do |file|
        file.read.should == "@import '#{banner.template}_banner';"
      end
    end

    it 'should generate correct js manifest file' do
      File.open "#{fixtures_dir}#{banner.template}_banner_manifest.js" do |file|
        file.read.should == "//= require ./banners/#{banner.template}_banner"
      end
    end

    it 'should generate correct coffee file' do
      File.open "#{fixtures_dir}banners/#{banner.template}_banner.js.coffee" do |file|
        file.readline.should == "class Banners.Banner_1Banner extends Banners.Base\n"
        file.readline.should == "$('.#{banner.template}-banner').each (index, element)-> new Banners.#{banner.template.capitalize}Banner($(element)).run()"
      end
    end

    it 'should generate correct scss file' do
      File.open "#{fixtures_dir}_#{banner.template}_banner.css.scss" do |file|
        file.read.should == ".#{banner.template}-banner{}"
      end
    end
  end
end
