class AddTemplateToBanners < ActiveRecord::Migration
  def change
    add_column :banners, :template, :string
  end
end
