class RemoveBannersUserIndex < ActiveRecord::Migration
  def up
    remove_index :banners, name: "index_banners_on_user_id"
  end

  def down
    add_index "banners", ["user_id"], :name => "index_banners_on_user_id", :unique => true
  end
end
