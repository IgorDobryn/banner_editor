class CreateBannerAttributes < ActiveRecord::Migration
  def change
    create_table :banner_attributes do |t|
      t.integer :banner_id, null: false
      t.string :name, null: false
      t.string :value, null: false

      t.timestamps
    end

    add_index :banner_attributes, :name
    add_index :banner_attributes, [:banner_id, :name], unique: true
    add_foreign_key :banner_attributes, :banners
  end
end
