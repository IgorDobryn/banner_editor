class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.integer :user_id, null: false

      t.timestamps
    end

    add_index :banners, :user_id, unique: true
    add_foreign_key :banners, :users
  end
end
