desc "Create banner template for pointed id."
task create_banner_template: [:environment] do
  TemplateGenerator.new(ENV['id']).generate!
end
