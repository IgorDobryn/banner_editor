desc "Populate test data."
task add_test_data: ['environment', 'db:drop', 'db:create', 'db:migrate', 'db:seed'] do
  users = FactoryGirl.create_list :user, 3

  users.each_with_index do |user, index|
    FactoryGirl.create :banner, user: user, attributes: {message: "Test banner #{index}", width: "#{320 + rand(30)}", height: "#{280 + rand(30)}"}
    FactoryGirl.create :banner, user: user, attributes: {message: "Test banner #{index}", width: "#{320 + rand(30)}", height: "#{100 + rand(30)}"}
  end
end
